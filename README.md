**Installation guidelines:**
To install this as chrome extension, download this repository in your local machine and follow the steps:

- Open chrome browser and go to chrome://extensions/
- Enable Developer Mode from top right corner. This will make additional buttons visible on the top left side.
- Click on "Load Unpacked" button and select the downloaded folder and click done. (Should be dist folder and the selected folder must have manifest.json file).

**How to use:**
Once extension is installed, you can view it from the top right corner of your browser window.
You can pin the extension so it is always visible after the address bar.
